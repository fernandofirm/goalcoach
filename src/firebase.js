import * as firebase from 'firebase';


const config = {
    apiKey: "AIzaSyD9Wah-OU4yF5TOPf4OD7xita-Q6MAroWQ",
    authDomain: "goalcoach-bd200.firebaseapp.com",
    databaseURL: "https://goalcoach-bd200.firebaseio.com",
    projectId: "goalcoach-bd200",
    storageBucket: "goalcoach-bd200.appspot.com",
    messagingSenderId: "924236152516"
  };
  
export const firebaseApp = firebase.initializeApp(config);
export const goalRef = firebase.database().ref('goals');
export const completeGoalRef = firebase.database().ref('completeGoals');
